# Augeron

Replace the serifs of Courier New with rings.

## Design notes

Design Grid of 2048 UPM.

Compared to a mostly standard mono stroke-width of 84,
the ring is designed as a perfect circle with circle with
an inner circle of diameter 88 and the stroked ring with an
_additional_ thickness of 80.
Hence, geometrically the hole is slightly larger than the
standard stroke and the circular stroke is slightly thinner.

# END
